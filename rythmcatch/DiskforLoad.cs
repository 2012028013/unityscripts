﻿using UnityEngine;
using System.Collections;

public class DiskforLoad : MonoBehaviour
{

    Camera _mainCam = null;

    public static bool turnL = false;
    private static bool _mouseState;
    private Vector2 zero_point;
    private Vector2 old_point;
    private Vector2 new_point;

    /// <summary>
    /// 마우스가 다운된 오브젝트
    /// </summary>
    private GameObject target, disk_center;
    /// <summary>
    /// 마우스 좌표
    /// </summary>
    private Vector3 MousePos;
    private Vector3 screenPos;

    // Use this for initialization
    void Awake()
    {
        _mainCam = Camera.main;
    }

    void start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        disk_center = GameObject.Find("Center");
        screenPos = _mainCam.WorldToScreenPoint(disk_center.transform.position);
        Debug.Log(screenPos);

        //원점
        zero_point.x = screenPos.x;
        zero_point.y = screenPos.y;

        //마우스가 내려갔는지?
        if (true == Input.GetMouseButtonDown(0))
        {
            //내려갔다.

            //타겟을 받아온다.
            target = GetClickedObject();

            //타겟이 나인가?
            if (true == target.Equals(gameObject))
            {
                //있으면 마우스 정보를 바꾼다.

                //시작점 setting
                old_point.x = Input.mousePosition.x;
                old_point.y = Input.mousePosition.y;

                _mouseState = true;
            }

        }
        else if (true == Input.GetMouseButtonUp(0))
        {
            //마우스가 올라 갔다.
            //마우스 정보를 바꾼다.
            _mouseState = false;
        }

        if (true == _mouseState)
        {
            //눌렸다!

            //둘째점 위치
            new_point.x = Input.mousePosition.x;
            new_point.y = Input.mousePosition.y;
            //Debug.Log(new_point.x);
            //Debug.Log(new_point.y);

            //회전각 구하기 -> 내적/두 변의 길이 곱 = cos(theta) -> acos(cos(theta))-> theta

            //원점기준으로 평행이동한 벡터

            Vector2 trans_point1, trans_point2;
            trans_point1.x = (new_point.x - zero_point.x); trans_point1.y = (new_point.y - zero_point.y);
            trans_point2.x = (old_point.x - zero_point.x); trans_point2.y = (old_point.y - zero_point.y);

            /*//theta 구하는 건데 필요 할지도 모른다
            float dot_product = trans_point1.x * trans_point2.x + trans_point1.y * trans_point2.y; //내적값

            float length1 = Mathf.Sqrt(Mathf.Pow(trans_point1.x, 2) + Mathf.Pow(trans_point1.y, 2));

            float length2 = Mathf.Sqrt(Mathf.Pow(trans_point2.x, 2) + Mathf.Pow(trans_point2.y, 2));

            float cos_theta = dot_product / (length1 * length2);

            float theta = Mathf.Acos(cos_theta) * 180 / Mathf.PI;
            */

            //1사분면
            if (trans_point1.x > 0 && trans_point1.y > 0)
            {
                //Debug.Log("1side");
                if (trans_point1.x - trans_point2.x < 0 && trans_point1.y - trans_point2.y > 0)
                {
                    //Debug.Log("counter_clockwise");
                    turnL = true;

                }
                else if (trans_point1.x - trans_point2.x > 0 && trans_point1.y - trans_point2.y < 0)
                {
                    //Debug.Log("clockwise");
                    turnL = false;

                }
                else//변화량 0일때
                {
                    //Debug.Log("idle\n");

                }
            }
            //2사분면
            else if (trans_point2.x < 0 && trans_point1.y > 0)
            {
                //Debug.Log("2side");
                if (trans_point1.x - trans_point2.x < 0 && trans_point1.y - trans_point2.y < 0)
                {
                    //Debug.Log("counter_clockwise");
                    turnL = true;

                }
                else if (trans_point1.x - trans_point2.x > 0 && trans_point1.y - trans_point2.y > 0)
                {
                    //Debug.Log("clockwise");
                    turnL = false;

                }
                else//변화량 0일때
                {
                    //Debug.Log("idle\n");

                }
            }
            //3사분면
            else if (trans_point1.x < 0 && trans_point1.y < 0)
            {
                //Debug.Log("3side");
                if (trans_point1.x - trans_point2.x > 0 && trans_point1.y - trans_point2.y < 0)
                {
                    //Debug.Log("counter_clockwise");
                    turnL = true;

                }
                else if (trans_point1.x - trans_point2.x < 0 && trans_point1.y - trans_point2.y > 0)
                {
                    //Debug.Log("clockwise");
                    turnL = false;

                }
                else//변화량 0일때
                {
                    //Debug.Log("idle\n");

                }
            }
            //4사분면
            else if (trans_point1.x > 0 && trans_point1.y < 0)
            {
                //Debug.Log("4side");
                if (trans_point1.x - trans_point2.x > 0 && trans_point1.y - trans_point2.y > 0)
                {
                    //Debug.Log("counter_clockwise");
                    turnL = true;

                }
                else if (trans_point1.x - trans_point2.x < 0 && trans_point1.y - trans_point2.y < 0)
                {
                    // Debug.Log("clockwise");
                    turnL = false;

                }
                else//변화량 0일때
                {
                    // Debug.Log("idle\n");

                }
            }

            old_point.x = new_point.x;
            old_point.y = new_point.y;

        }
        else
        {
            Debug.Log("idle\n");
        }

    }

    public static bool isMoving()
    {
        if (_mouseState)
            return true;
        else
            return false;

    }

    private GameObject GetClickedObject()
    {
        //충돌이 감지된 영역
        RaycastHit hit;
        //찾은 오브젝트
        GameObject target = null;

        //마우스 포인트 근처 좌표를 만든다.
        Ray ray = _mainCam.ScreenPointToRay(Input.mousePosition);

        //마우스 근처에 오브젝트가 있는지 확인
        if (true == (Physics.Raycast(ray.origin, ray.direction * 10, out hit)))
        {
            //있다!

            //있으면 오브젝트를 저장한다.
            target = hit.collider.gameObject;
        }

        return target;
    }
}