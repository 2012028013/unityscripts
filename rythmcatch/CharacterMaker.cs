﻿using UnityEngine;
using System.Collections;

public class CharacterMaker : MonoBehaviour {

    public GameObject[] Characters;
    public int CharacterNumber;
    public static GameObject playerCharacter;

    void Awake()
    {

    }
	// Use this for initialization
	void Start () {
        CharacterNumber = GameStatus.CharacterNo;
        playerCharacter = Instantiate(Characters[CharacterNumber]);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
