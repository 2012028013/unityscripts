﻿using UnityEngine;
using System.Collections;

public class BackGroundMusic : MonoBehaviour {

    public AudioClip[] Musics;
    private GameObject BGM;
    private int index;

    void Awake()
    {
        BGM = GameObject.Find("BackGroundMusic");
        
    }
	// Use this for initialization
	void Start () {
        index = Random.Range(0, Musics.Length);
        Debug.Log(index);
        BGM.GetComponent<AudioSource>().clip = Musics[index];
        BGM.GetComponent<AudioSource>().Play();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
