﻿using UnityEngine;
using System.Collections;

public class Note : MonoBehaviour
{
    private Vector3 movement;    //NoteGenerator에서 위치값 받아올것임
    public static int speed = 6;
    private UIManager ui;
    QuerySDEmotionalController emotion;

    // Use this for initialization
    void Start()
    {
        ui = GameObject.Find("Canvas").GetComponent<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {
         movement = this.transform.position;

        movement.z -= speed * Time.deltaTime;

        this.transform.position = movement;
    }

    void OnTriggerEnter(Collider Col)
    {
        ui.ScoreUI(GameStatus.Score);
        if (Col.tag == "Bar")
        {
            //event -> fail
            GameStatus.Combo = 0;
            ui.ComboUI(0);
            GameStatus.Fail++;
            GameStatus.Current_HP -= GameStatus.Damage;
            ui.HealthGUI(GameStatus.Current_HP);
        }
        if (Col.tag == "Player")
        {
            //event -> success
            GameStatus.Combo++;
            ui.ComboUI(GameStatus.Combo);
            GameStatus.Catch++;
            GameStatus.Score += 100;
            if (GameStatus.Current_HP < GameStatus.MAX_HP)
            {
                GameStatus.Current_HP++;
                ui.HealthGUI(GameStatus.Current_HP);
            }
        }
    }
}