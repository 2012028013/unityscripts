﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverMenu : MonoBehaviour {

    public Button ReGame;
    public Button EndGame;
    public Button Option;
    public Text ButtonText;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void PressButton(int nButton)
    {
        switch(nButton)
        {
            case 1:
                //Regame
                GameStatus.reset(); //체력 다시 100으로 해줘야 실행하자마자 종료되지 않음
                if (GameStatus.getSCside() == 'R')
                {
                    Application.LoadLevel("GamePlayRightSC");
                }
                else
                {
                    Application.LoadLevel("GamePlayLeftSC");
                }
                break;
            case 2:
                //Endgame
                Application.Quit();
                return;
            case 3:
                //Option
                GameStatus.ChangeSide();
                if (GameStatus.getSCside() == 'R')
                {
                    ButtonText.text = "SCside: R";
                }
                else
                {
                    ButtonText.text = "SCside: L";
                }
                break;
            case 4:
                //Music Select
                Application.LoadLevel("selectmusic");
                break;
            default:
                break;
        }
    }
}
