﻿using UnityEngine;
using System.Collections;

public class NoteGenerate : MonoBehaviour
{
    private GameObject point;
    private Transform notemakerposition;
    private Vector3 GenMove;
    public GameObject Note;
    private float GameTime =0;
    

    // Use this for initialization
    void Start() {        
        point = GameObject.Find("NoteGenerator");
        GenMove = point.GetComponent<Transform>().position;
        notemakerposition = point.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update() {
        GameTime += Time.deltaTime;
        
        if (GameStatus.getSCside() == 'L')
        {
            GenMove.x = Random.Range(1, 20);
        }
        else
        {
            GenMove.x = Random.Range(-20, -1);
        }
        notemakerposition.position = GenMove;

        if(GameTime < GameStatus.MusicEndTime-3)
            InvokeRepeating("Spawn", Random.Range(0.4f, 1.2f), 10f); //이부분에서 다음노트사이의 시간 간격을 정함
    }

    void Spawn()
    {
        Instantiate(Note, notemakerposition.position, Quaternion.identity);
        CancelInvoke();
    }
}