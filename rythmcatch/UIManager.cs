﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour {

    public Slider healthSlider;
    public Text ScoreText;
    public Text ComboText;
    public Text CharSpdText;
    public Text NoteSpdText;

	// Update is called once per frame
	void Update () {
        
        HealthGUI(GameStatus.Current_HP);
        ComboUI(GameStatus.Combo);
        ScoreUI(GameStatus.Score);
        CharSpdUI(DiskMoving.spd_amt);
        NoteSpdUI(Note.speed);
    }

    public void HealthGUI(int health)
    {
        healthSlider.value = health;
    }

    public void ScoreUI(int Scorevalue)
    {
        ScoreText.text = "Score: " + Scorevalue;
    }

    public void ComboUI(int Combovalue)
    {
        ComboText.text = "Combo: " + Combovalue;
    }

    public void CharSpdUI(int Speedvalue)
    {
        CharSpdText.text = "MovingSpeed: " + Speedvalue;
    }

    public void NoteSpdUI(int Speedvalue)
    {
        NoteSpdText.text = "NoteSpeed: " + Speedvalue;
    }
}
