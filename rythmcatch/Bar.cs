﻿using UnityEngine;
using System.Collections;

public class Bar : MonoBehaviour {

    private static float maxLength;
    private static float minLength;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        setMaxL();
        setMinL();
	}

    void OnTriggerEnter(Collider Col)
    {
        if (Col.tag == "Note")
        {
            //event -> success
            Destroy(Col.gameObject);
                       
        }
    }

    private static void setMaxL()
    {
        if(GameStatus.getSCside() == 'L')
        {
            maxLength = 20;
        }
        else
        {
            maxLength = -1;
        }
    }

    private static void setMinL()
    {
        if (GameStatus.getSCside() == 'L')
        {
            minLength = 0.5f;
        }
        else
        {
            minLength = -20;
        }
    }

    public static float getMaxL()
    {
        return maxLength;
    }

    public static float getMinL()
    {
        return minLength;
    }
}
