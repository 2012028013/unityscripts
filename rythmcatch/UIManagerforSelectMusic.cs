﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManagerforSelectMusic : MonoBehaviour {

    public Text DiffButtonText;
    public Text SCsideText;
    public Text CharNum;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        setDiffTextbutton();
        setSCsideTextbutton();
        setCharNum();
	}

    public void setDiffTextbutton()
    {
        if(GameStatus.Difficulty == 0)
        {
            DiffButtonText.text = "Easy";
        }
        else if(GameStatus.Difficulty == 1)
        {
            DiffButtonText.text = "Normal";
        }
        else if(GameStatus.Difficulty == 2)
        {
            DiffButtonText.text = "Hard";
        }
        else
        {
            DiffButtonText.text = "Super Hard";
        }
    }

    public void setSCsideTextbutton()
    {
        if (GameStatus.getSCside() == 'R')
        {
            SCsideText.text = "Right";
        }
        else
        {
            SCsideText.text = "Left";
        }
    }

    public void setCharNum()
    {
        CharNum.text = "Character No." + GameStatus.CharacterNo;
    }
}

/*

*/
