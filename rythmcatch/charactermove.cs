﻿using UnityEngine;
using System.Collections;

public class charactermove : MonoBehaviour {

    private Quaternion Right = Quaternion.identity;
    private Quaternion Left = Quaternion.identity;
    private const int MAX_HP = 100;

	// Use this for initialization
	void Start () {
        Right.eulerAngles = new Vector3(300, 78, 101);
        Left.eulerAngles = new Vector3(300, 265, 275);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(DiskMoving.turnL)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Left, Time.deltaTime * 5.0f);
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Right, Time.deltaTime * 5.0f);
        }
        Debug.Log(GameStatus.Combo);
    }

    void OnTriggerEnter(Collider Col)
    {
        if (Col.tag == "Note")
        {
            //event -> success
            Destroy(Col.gameObject);
           
        }
    }

    public void SpeedUp()
    {
        if (DiskMoving.spd_amt < 200)
        {
            DiskMoving.spd_amt += 10;
        }
    }

    public void SpeedDown()
    {
        if (DiskMoving.spd_amt > 20)
        {
            DiskMoving.spd_amt -= 10;
        }
    }
}
