﻿using UnityEngine;
using System.Collections;

public class NoteGenerate : MonoBehaviour
{
    private GameObject point;
    private Transform notemakerposition;
    public GameObject Note;
    

    // Use this for initialization
    void Start() {
        point = GameObject.Find("NoteGenerator");
    }

    // Update is called once per frame
    void Update() {
        notemakerposition = point.GetComponent<Transform>();
        InvokeRepeating("Spawn", 1, 10f);
    }

    void Spawn()
    {
        Instantiate(Note, notemakerposition.position, Quaternion.identity);
        CancelInvoke();
    }
}