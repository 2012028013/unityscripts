﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameStartMenu : MonoBehaviour {

    public Button StartGame;
    public Button EndGame;
    public Text ButtonText;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PressButton(int nButton)
    {
        switch (nButton)
        {
            case 1:
                //Startgame
                Application.LoadLevel("selectmusic");
                break;
            case 2:
                //Endgame
                Application.Quit();
                return;
            default:
                break;
        }
    }
}
