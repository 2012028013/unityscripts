using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

using System.Runtime;
using System.Runtime.InteropServices;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

using NAudio;
using NAudio.Wave;



public class MusicPlayer : MonoBehaviour
{
    ///////////////////////////////////////////////////////////////////////////
    #region Variables

    private static MusicPlayer mInstance = null;

    private string cLocalPath = "file://localhost/"; //localhost지정 안해주면 음악 실행 안됨
    public string[] FileName;
    public string MP3FileName;
    public Text[] ErrorCheck = new Text[20];

    private IWavePlayer mWaveOutDevice;
    private WaveStream mMainOutputStream;
    private WaveChannel32 mVolumeStream;


    #endregion
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    #region Interface

    public MusicPlayer()
    {
        mInstance = this;
    }

    public void Awake()
    {
        //MP3FileName = "C:\\Users\\lucid\\Desktop\\EZMP3\\Makes+Me+Wonder.mp3";
        //setMusicEndTime(MP3FileName);

        FileName = Directory.GetFiles(Directory.GetCurrentDirectory()+"/storage/sdcard0/Music/Ez2dj");
        
        //setMusicEndTime(FileName[0]);
        for (int i = 0; i < 20; i++)
            ErrorCheck[i].text = FileName[i];
    }

    public void Start()
    {
        UnloadAudio();  //이미 실행중이던 음악 끄고
        LoadAudio();    //새로 선택한 음악 실행     
    }

    public void Update()
    {
        
    }

    public void OnDestroy()
    {
        UnloadAudio();//씬 끝날때 호출됨
    }

    public static void setMusicEndTime(string FN)
    {
        GameStatus.MusicEndTime = AudioDuration(FN);
    }

    public string pathForDocumentsFile(string filename)
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(Path.Combine(path, "Documents"), filename);
        }

        else if (Application.platform == RuntimePlatform.Android)
        {
            string path = Application.persistentDataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(path, filename);
        }

        else
        {
            string path = Application.dataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(path, filename);
        }
    }

    private static int AudioDuration(string FileFullPath)
    {
        TagLib.File file = TagLib.File.Create(FileFullPath);
        int s_time = (int)file.Properties.Duration.TotalSeconds;
        int s_minutes = s_time / 60;
        int s_seconds = s_time % 60;
        //return s_minutes.ToString() + ":" + s_seconds.ToString() + " minutes";

        return s_time;
    }

    public void OnApplicationQuit()
    {
        UnloadAudio();  //어플 끌때 음악 자동으로 종료됨
    }

    #endregion
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    #region Implementation
    
    private void LoadAudio()
    {
        //system.windows.forms.openfiledialog ofd = new system.windows.forms.openfiledialog();
        ////위 문장은 windows 전용이라 안드로이드에서 실행 불가능
        //ofd.title = "open audio file";
        //ofd.filter = "mp3 audio (*.mp3) | *.mp3";
        //if (ofd.showdialog() == system.windows.forms.dialogresult.ok)
        
            //WWW www = new WWW(cLocalPath + MP3FileName);
            WWW www = new WWW(cLocalPath + FileName[0]);
            
            while (!www.isDone) { };
            if (!string.IsNullOrEmpty(www.error))
            {
                //System.Windows.Forms.MessageBox.Show("Error! Cannot open file: " + FileName + "; " + www.error);
                return;
            }

            byte[] imageData = www.bytes;

            if (!LoadAudioFromData(imageData))
            {
                //System.Windows.Forms.MessageBox.Show("Cannot open mp3 file!");
                return;
            }

            mWaveOutDevice.Play();

            Resources.UnloadUnusedAssets();
        

    }

    private bool LoadAudioFromData(byte[] data)
    {
        try
        {
            MemoryStream tmpStr = new MemoryStream(data);
            mMainOutputStream = new Mp3FileReader(tmpStr);
            mVolumeStream = new WaveChannel32(mMainOutputStream);

            mWaveOutDevice = new WaveOut();
            mWaveOutDevice.Init(mVolumeStream);

            return true;
        }
        catch (System.Exception ex)
        {
            Debug.LogWarning("Error! " + ex.Message);
        }

        return false;
    }

    private void UnloadAudio()
    {
        if (mWaveOutDevice != null)
        {
            mWaveOutDevice.Stop();
        }
        if (mMainOutputStream != null)
        {
            // this one really closes the file and ACM conversion
            mVolumeStream.Close();
            mVolumeStream = null;

            // this one does the metering stream
            mMainOutputStream.Close();
            mMainOutputStream = null;
        }
        if (mWaveOutDevice != null)
        {
            mWaveOutDevice.Dispose();
            mWaveOutDevice = null;
        }

    }

    #endregion
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    #region Properties

    private static MusicPlayer Instance
    {
        get
        {
            return mInstance;
        }
    }

    #endregion
    ///////////////////////////////////////////////////////////////////////////
}
