﻿//moving generator randomly
using UnityEngine;
using System.Collections;

public class NoteGenerate : MonoBehaviour
{
    private GameObject point;
    private Transform notemakerposition;
    private Vector3 GenMove;
    public GameObject Note;
    

    // Use this for initialization
    void Start() {
        point = GameObject.Find("NoteGenerator");
        GenMove = point.GetComponent<Transform>().position;
    }

    // Update is called once per frame
    void Update() {
        notemakerposition = point.GetComponent<Transform>();
        GenMove.x = Random.Range(1.0f, 20.0f);
        notemakerposition.position = GenMove;
        InvokeRepeating("Spawn", 1, 10f);
    }

    void Spawn()
    {
        Instantiate(Note, notemakerposition.position, Quaternion.identity);
        CancelInvoke();
    }
}