﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

using System.Runtime;
using System.Runtime.InteropServices;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

public class AudioControl : MonoBehaviour {

    public string url;
    private string path;
    private string[] MusicBundles;
    public static AudioSource source;
    public static string MP3filePath;
    private int musicnum;
    
    void Awake()
    {
        path = MP3filePath;
        
        MusicBundles = Directory.GetFiles(path);

        musicnum = Random.Range(0, Directory.GetFiles(path).Length-1);

        Debug.Log(Directory.GetFiles(path).Length);

        setMusicEndTime(MusicBundles[musicnum]);
    }

    void Start()
    {
        url = "file:///" + MusicBundles[musicnum];  //file://localhost/로 했을땐 안됬었음 알아 둘 것

        Debug.Log(url);
        
        WWW www = new WWW(url);

        while(!www.isDone)
        {
            ;//다운 받을때 까지 기다림
        }

        source = GetComponent<AudioSource>();
        source.clip = www.audioClip;
            source.Play();
    }
    void Update()
    {
        

    }

    public static void setMusicEndTime(string FN)
    {
        GameStatus.MusicEndTime = AudioDuration(FN);
        Debug.Log(GameStatus.MusicEndTime);
    }

    private static int AudioDuration(string FileFullPath)
    {
        TagLib.File file = TagLib.File.Create(FileFullPath);
        int s_time = (int)file.Properties.Duration.TotalSeconds;

        return s_time;
    }

    public static void pauseMusic()
    {
        source.Pause();
    }

    public static void resumeMusic()
    {
        source.UnPause();
    }
}