﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class GameStatus : MonoBehaviour {

    public static int Combo = 0;
    public static int MaxCombo = 0;
    public static int Fail = 0;
    public static int Catch = 0;
    public const int MAX_HP = 100;
    public static int Current_HP = 100;
    public const int Damage = 10;
    public static int Score = 0;
    public static int MusicEndTime;
    private static char SCsideL = 'R';
    private float clearTime = 9999;
    public GameObject NoteGen;
    public static int Difficulty = 0;   //0: easy, 1: Normal, 2: hard, 3: super hard
    private int spdDownLimit = 3;
    private float ChangeSpeedTime;
    public static string path = Directory.GetCurrentDirectory() + "storage/sdcard0/Music";
    public static int CharacterNo = 0;
    private bool buttonclicked = false;
    private Rect windowRect;
    private int x;
    private int y;

    void Awake()
    {

    }
    // Use this for initialization
    void Start () {
        x = Screen.width;
        y = Screen.height;
        windowRect = new Rect(x / 5, y / 5, x / 3f, y / 2f);

        ChangeSpeedTime = Random.Range(1f, 10f);
        clearTime = MusicEndTime + 5;

        for (int i = 0; i < Difficulty; i++)
        {
            Instantiate(NoteGen);
        }
	}

    void OnGUI()
    {
        if (buttonclicked)
        {
            windowRect = GUI.Window(0, windowRect, DoMyWindow, "Pause");
        }
    }

    void DoMyWindow(int windowID)
    {
        //GUI.Label(new Rect(x / 5, y / 24f, x, y), "...");

        if (GUILayout.Button("Resume", GUILayout.Width(x / 3.3f), GUILayout.Height(y / 11)))
        {
            Time.timeScale = 1;
            AudioControl.resumeMusic();
            buttonclicked = false;
        }
        GUILayout.Label("");
        if (GUILayout.Button("Select Music", GUILayout.Width(x / 3.3f), GUILayout.Height(y / 11)))
        {
            reset();
            Time.timeScale = 1;
            Application.LoadLevel("selectmusic");
        }
        GUILayout.Label("");
        if (GUILayout.Button("Quit", GUILayout.Width(x / 3.3f), GUILayout.Height(y / 11)))
        {
            Application.Quit();
        }

            GUI.DragWindow(new Rect(0, 0, 10000, 10000));
    }

    // Update is called once per frame
    void Update ()
    {
        if(!(Application.loadedLevel == 2 || Application.loadedLevel == 3))
        {
            clearTime = 9999;//INF: GameOver 화면넘어감 방지
        }
        if(Combo > MaxCombo)
        {
            MaxCombo = Combo;
        }

        if(clearTime > 0 )
        {
            clearTime -= Time.deltaTime;            
        }
        else
        {
            Application.LoadLevel("result");
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            if (Application.loadedLevelName == "selectmusic")
            {
                Application.LoadLevel("GameStart");
            }
            else if(Application.loadedLevelName == "result")
            {
                SelectMusic();
            }
            else if(Application.loadedLevelName == "GameStart")
            {
                Application.Quit();
            }
            else if(Application.loadedLevelName == "GameOver")
            {
                SelectMusic();
            }
            else
            {
                Time.timeScale = 0; //일시정지->재개는 timeScale = 1로 바꿔주면 됨
                                    //종료 or 재선곡 or 속행
                AudioControl.pauseMusic();
                buttonclicked = true;
            }
        }
        

        if(Current_HP < 1 )
        {
            Debug.Log("GameOver");
            Current_HP = 100;
            Application.LoadLevel("GameOver");  //>>바로 다음 씬(GameOver)으로 가기 때문에 Dead모션 만들면 그 후로 옮길것
        }

        if(Difficulty == 3)
        {
            if (ChangeSpeedTime < 0)
            {
                Note.speed = Random.Range(6, 20);
                ChangeSpeedTime = Random.Range(1f, 10f);
            }
            else
            {
                ChangeSpeedTime -= Time.deltaTime;
            }
        }
	
	}

    public static void reset()
    {
        GameStatus.Catch = 0;
        GameStatus.Fail = 0;
        GameStatus.Score = 0;
        GameStatus.Combo = 0;
        GameStatus.Current_HP = 100;
    }

    public static void setLSC()
    {
        SCsideL = 'L';
    }

    public static void setRSC()
    {
        SCsideL = 'R';
    }

    public static char getSCside()
    {
        return SCsideL;
    }

    public static void ChangeSide()
    {
        if(getSCside() == 'L')
        {
            setRSC();
        }
        else
        {
            setLSC();
        }
    }

    public void NoteSpeedUP()   //주의 static 쓰면 제대로 작동안함
    {
        if (Note.speed < 25)
            Note.speed++;
    }

    public void NoteSpeedDown()
    {
        if (Note.speed > spdDownLimit)
            Note.speed--;
    }

    public void setDiff()
    {
        Difficulty++;
        if(Difficulty > 3)
        {
            Difficulty = 0;
        }

        if(Difficulty == 0)
        {
            spdDownLimit = 3;
        }
        else if(Difficulty == 1)
        {
            spdDownLimit = 6;
        }
        else if(Difficulty == 2)
        {
            spdDownLimit = 9;
        }
        else
        {
            spdDownLimit = 13;     
        }
        Note.speed = spdDownLimit;
    }

    public void ChangeSC()
    {
        ChangeSide();
    }

    public void ChangeCharacterR()
    {
        if (CharacterNo >= 13)
        {
            return;
        }
        else
            CharacterNo++;
    }

    public void ChangeCharacterL()
    {
        if (CharacterNo <= 0)
        {
            return;
        }
        else
            CharacterNo--;
    }

    public void SelectMusic()
    {
        reset();
        Application.LoadLevel("selectmusic");
    }
}
