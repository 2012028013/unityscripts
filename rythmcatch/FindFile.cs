﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class FindFile : MonoBehaviour {

    private List<string> fileNames = new List<string>();
    private List<string> fullNames = new List<string>();
    private List<string> DirNames = new List<string>();
    public static string path;
    private Vector2 scrollPos;
    private Vector2 scrollPosforPopup;
    private bool buttonclicked = false;
    private bool resetbuttonclicked = false;
    private int x;
    private int y;
    private string oldpath;
    public Rect windowRect;
    private char[] delemiters = { '/', '\\' };

    void Awake()
    {
        
    }
    void OnGUI()
    {        
        if (buttonclicked)
        {            
            windowRect = GUI.Window(0, windowRect, DoMyWindow, "Change Directory");
        }

        Font font = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
        GUI.skin.label.font = GUI.skin.button.font = GUI.skin.box.font = font;
        GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = x/40;

        scrollPos = GUILayout.BeginScrollView(scrollPos, GUILayout.Width(x/2), GUILayout.Height(y) );
        
        GUILayout.Label("Select Music want you wanna DJing~");

        for (int i = 0; i < fileNames.Count; i++)
        {
            if (GUILayout.Button(fileNames[i], GUILayout.Width(x/2.3f), GUILayout.Height(y/17)))
            {
                AudioControl.MP3filePath = fullNames[i];
                if (GameStatus.getSCside() == 'R')
                {
                    Application.LoadLevel("GamePlayRightSC");
                }
                else
                {
                    Application.LoadLevel("GamePlayLeftSC");
                }
            }
        }

        GUILayout.EndScrollView();

    }
    // Use this for initialization
    void Start () {
        
        path = GameStatus.path;
        
        x = Screen.width;
        y = Screen.height;
        windowRect = new Rect(x / 5, y / 5, x / 1.1f, y / 1.5f);

        FileSearch(path);   //기본적으로 시작할때 한번 찾아본다
        DirSearch(path);
        oldpath = path;
    }

    void Update()
    {
        if(resetbuttonclicked)
        {
            resetlist();
            FileSearch(path);   //바뀐 path로 재 탐색
            DirSearch(path);
            buttonclicked = true;
            resetbuttonclicked = false;
        }
    }

    void DirSearch(string sDir)
    {
        try
        {
            foreach (string d in Directory.GetDirectories(sDir))
            {
                string cpy = d.Replace(sDir, "");
                cpy = cpy.Substring(1, cpy.Length - 1);
                DirNames.Add(cpy);
            }
        }
        catch (System.Exception excpt)
        {
            Debug.Log(excpt.Message);
        }
    }

    void FileSearch(string filepath)
    {
        try
        {
            foreach (string f in Directory.GetFiles(filepath, "*.mp3"))
            {
                string cpy = f.Replace(filepath, "");
                cpy = cpy.Substring(1, cpy.Length - 1);
                fileNames.Add(cpy);
                fullNames.Add(f);
            }
        }
        catch (System.Exception excpt)
        {
            Debug.Log(excpt.Message);
        }
    }

    void DoMyWindow(int windowID)
    {
        GUI.Label(new Rect(x/5, y/24f, x, y),"Current: " + path);

        ScrollViewSet();

        if (GUI.Button(new Rect(x/1.5f, y/1.7f, x/10, y/20), "Cancel"))
        {
            path = oldpath;
            resetlist();
            FileSearch(path);   //바뀐 path로 재 탐색
            DirSearch(path);
            buttonclicked = false;
        }
        if (GUI.Button(new Rect(x/2, y/1.7f, x/10, y/20), "Ok"))
        {
            GameStatus.path = path;
            oldpath = path;
            buttonclicked = false;
        }

        GUI.DragWindow(new Rect(0, 0, 10000, 10000));
    }

    public void ButtonClicked()
    {
        buttonclicked = true;
    }

    private void resetlist()
    {
        fileNames.Clear();
        fullNames.Clear();
        DirNames.Clear();
    }

    private void resetwindow()
    {
        GUILayout.EndScrollView();
        buttonclicked = false;
        resetbuttonclicked = true;
    }

    private void ScrollViewSet()
    {
        scrollPosforPopup = GUILayout.BeginScrollView(scrollPosforPopup, GUILayout.Width(x / 2), GUILayout.Height(y / 1.8f));
        GUILayout.Label("Directory Files");

        string[] Dirnamesplit = path.Split(delemiters);

        if (DirNames.Count == 0)
        {
            GUILayout.Label("No more Directory files");
            if (GUILayout.Button("MoveBack(..)", GUILayout.Width(x / 2.3f), GUILayout.Height(y / 17)))
            {
                path = path.Replace(Dirnamesplit[Dirnamesplit.Length - 1], "");
                while (path[path.Length - 1] == '/' || path[path.Length - 1] == '\\')
                {
                    path = path.Remove(path.Length - 1);
                }
                GameStatus.path = path;
                resetwindow();
            }
        }
        else
        {
            if (GUILayout.Button("MoveBack(..)", GUILayout.Width(x / 2.3f), GUILayout.Height(y / 17)))
            {
                path = path.Replace(Dirnamesplit[Dirnamesplit.Length - 1], "");
                while (path[path.Length - 1] == '/' || path[path.Length - 1] == '\\')
                {
                    path = path.Remove(path.Length - 1);
                }
                GameStatus.path = path;
                resetwindow();
            }
            for (int i = 0; i < DirNames.Count; i++)
            {
                if (GUILayout.Button(DirNames[i], GUILayout.Width(x / 2.3f), GUILayout.Height(y / 17)))
                {
                    path += "/" + DirNames[i];
                    GameStatus.path = path;
                    resetwindow();
                }
            }
        }

        GUILayout.EndScrollView();
    }
}
