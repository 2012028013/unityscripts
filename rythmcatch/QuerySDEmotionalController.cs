using UnityEngine;
using System.Collections;

public class QuerySDEmotionalController : MonoBehaviour {

	[SerializeField]
	Material[] emotionalMaterial;

	[SerializeField]
	GameObject queryFace;

    int cur_health;
    int old_com, new_com;

	public enum QueryChanSDEmotionalType
	{
		// Normal emotion
		NORMAL_DEFAULT = 0,
		NORMAL_ANGER = 1,
		NORMAL_BLINK = 2,
		NORMAL_GURUGURU = 3,
		NORMAL_SAD = 4,
		NORMAL_SMILE = 5,
		NORMAL_SURPRISE = 6

	}


	public void ChangeEmotion (QueryChanSDEmotionalType faceNumber)
	{
		queryFace.GetComponent<Renderer>().material = emotionalMaterial[(int)faceNumber];
	}
	
    //����: �׳� update()���� ChangeEmotion()�ϸ� MecanimController�� calling �ñ� ���Ƽ� ����
    void LateUpdate()
    {
        cur_health = GameStatus.Current_HP;
        new_com = GameStatus.Combo;

        if (GameStatus.Combo >= 100)
            ChangeEmotion(QueryChanSDEmotionalType.NORMAL_GURUGURU);
        else if (GameStatus.Combo >= 50)
            ChangeEmotion(QueryChanSDEmotionalType.NORMAL_SURPRISE);
        else if (GameStatus.Combo >= 10)
            ChangeEmotion(QueryChanSDEmotionalType.NORMAL_SMILE);
        else if (cur_health > GameStatus.MAX_HP / 2)
            ChangeEmotion(QueryChanSDEmotionalType.NORMAL_DEFAULT);
        else if (cur_health < GameStatus.MAX_HP / 2)
            ChangeEmotion(QueryChanSDEmotionalType.NORMAL_SAD);

        old_com = new_com;
    }
}
