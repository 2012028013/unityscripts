﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextManagerForResult : MonoBehaviour {

    public Text TotalNote;
    public Text Catch;
    public Text Fail;
    public Text MaxCombo;
    public Text Score;

    private bool resetStatus = false;

	// Use this for initialization
	void Start () {
        setTotalNote();
        setCatch();
        setFail();
        setMaxCombo();
        setScore();
        resetStatus = true;
    }
	
	// Update is called once per frame
	void Update () {
        //reset
        if (resetStatus)
        {
            GameStatus.MaxCombo = 0;
            GameStatus.Combo = 0;
            GameStatus.Current_HP = 100;
            GameStatus.Catch = 0;
            GameStatus.Fail = 0;
            GameStatus.Score = 0;
            resetStatus = false;
        }
        
	}
    public void setTotalNote()
    {
        TotalNote.text = "Total Notes: " + (GameStatus.Catch + GameStatus.Fail);
    }
    public void setCatch()
    {
        Catch.text = "Catch: " + GameStatus.Catch;
    }
    public void setFail()
    {
        Fail.text = "Fail: " + GameStatus.Fail;
    }
    public void setMaxCombo()
    {
        MaxCombo.text = "MaxCombo: " + GameStatus.MaxCombo;
    }
    public void setScore()
    {
        Score.text = "Score: " + GameStatus.Score;
    }
}
